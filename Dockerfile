FROM openjdk:8-jdk-alpine
WORKDIR /app
COPY ./target/Api-Investimentos-0.0.1-SNAPSHOT.jar .
ENTRYPOINT ["java","-jar","Api-Investimentos-0.0.1-SNAPSHOT.jar"]
EXPOSE 8080/tcp